<?php

/***************************************************************
* UW Groups Module                                            *
*                                                             *
* Created by the University of Washington Information School. *
* Be sure to read README and LICENSE.                         *
*
* For info about GWS:	
* https://wiki.cac.washington.edu/display/infra/Groups+Web+Service+REST+API
***************************************************************/

/**
 * @file
 * uwgroups shared module functions.
 */

//function Pre($s){echo '</pre>'.print_r($s,1).'</pre>';}

define('UW_GROUPS_URL', 'https://iam-ws.u.washington.edu/group_sws/v1/search?member=@NETID&type=effective&scope=all');


/**
 * Removes ALL roles for a user then uses the GWS
 * to fetch the user's groups then one-by-one adds them as roles.
 *
 * @param $user
 *   A user object.
 *
 * @return
 */
function _uw_groups_map_groups($user) {
	if (!is_object($user) || trim($user->name) == '')
		return;

	$users_groups = _uw_groups_fetch_groups($user->name);

	// ensure we got groups
	if (!$users_groups || empty($users_groups)) {
		_uw_groups_error('Could not get groups for user "%var".', $user->name);
		return;
	}

	_uw_groups_sync_roles($user->uid, $users_groups);
}


/**
 * Sync roles from database to those the UW groups the user is in
 *
 * @param $uid
 *	 A user object
 * @param $groups
 *	 An array of UW Groups the user is a member of
 */
function _uw_groups_sync_roles($uid, $groups){
	// Load user object
	$user = user_load($uid);

	// Get a list of valid roles
	$active_groups = user_roles(true);
	
	// Get a list of roles this user has
	$roles = $user->roles;
	
	// Remember if a change occurs
	$change = false; 
	
	// Step through the users roles and remove any that the user is no longer a UW Group member of
	foreach($roles as $rid => $role){
		// Ignore the 3 default Drupal roles
		if($rid <= 3)
			continue;
		
		// If the user is no longer in this UW Group, remove user from role.
		if(!in_array($role, $groups)){
			unset($roles[$rid]);
			$change = true;
		}
		
	}
	
	// Step through UW Groups that this user is a member of
	foreach ($groups as $group){
		// Skip it if the user already has this role
		if(in_array($group, $roles)){
			continue;
		}
		
		// Step through active groups
		foreach($active_groups as $rid => $role){
			// Ignore the 3 default Drupal roles
			if($rid <= 3)
				continue;
				
			if($role == $group){
				// Add the group to the user's roles array
				$roles[$rid] = $group;
				$change = true;
			}
		}
	}
		
	// If a change to user's roles occurred, then update the user
	if($change){
		// Save role changes to user
		user_save($user, array('roles' => $roles));
	}
	
}


/**
 * Logs an error in the Drupal log.
 */
function _uw_groups_error($msg, $var = NULL) {
	if ($var)
		watchdog('UWGWS', $msg, array('%var' => $var, ), WATCHDOG_ERROR);
	else
		watchdog('UWGWS', $msg, array(), WATCHDOG_ERROR);
}


/**
 * Query UW GWS for a list of NetID Groups that $netid is a member of
 *
 * @param $netid
 *   A string containing the NetID for which to fetch groups
 *
 * @return
 */
function _uw_groups_fetch_groups($netid){
	// Form the URL
	$url = str_replace('@NETID', $netid, UW_GROUPS_URL);
	
	// Init Curl
	$ch = curl_init();
	
	// If curl_init() failed, log error and return
	if ($ch == FALSE) {
		_uw_groups_error('curl_init() failed for user "%var".', $netid);
		return FALSE;
	}
	
	$cert_path = $_SERVER['DOCUMENT_ROOT'].'/'.drupal_get_path('module', 'uw_groups').'/';
	// Set curl Options
	// TODO: make it so key/cert/ca files are elsewhere on server, and a config screen sets where they are
	$options = array(
		CURLOPT_RETURNTRANSFER => TRUE,
		CURLOPT_HEADER         => FALSE,
		CURLOPT_BINARYTRANSFER => FALSE,
		CURLOPT_FOLLOWLOCATION => TRUE,
		CURLOPT_SSL_VERIFYHOST => TRUE,
		CURLOPT_SSL_VERIFYPEER => TRUE,
		CURLOPT_SSLCERT        => $cert_path.'cert.pem',
		CURLOPT_SSLKEY         => $cert_path.'key.pem',
		CURLOPT_CAINFO         => $cert_path.'uw_ca.pem',
		CURLOPT_VERBOSE        => FALSE,
		CURLOPT_URL            => $url,
	);
	
	// Set the options to the handler
	curl_setopt_array($ch, $options);
	
	// Do the curl call
	$data = curl_exec($ch);
	
	// If there are errors, log them and return false
	if (curl_errno($ch)) {
		$error = curl_error($ch);
		_uw_groups_error('curl_exec() failed with error "%var".', $error);
		
		return FALSE;
	}
	
	// Fetch the curl headers incase we want to use them
	$headers = curl_getinfo($ch);
	
	// Close the curl handler
	curl_close($ch);
	
	// If data isn't empty, extract the XML from it
	if (!empty($data)){
		$xml = simplexml_load_string(str_replace('xmlns=', 'ns=', $data));
	}

	// If we have XM
	if (isset($xml) && $xml != '') {
		$result = $xml->xpath("//a[@class='name']");
		$groups = array();
		
		while (list( , $node) = each($result)) {
			// we have to ensure $node[0] is a string
			$groups[] = (string)$node[0];
		}
	}
	// Log error
	else {
		_uw_groups_error('UW Groups could not parse group XML for ser "%var".', $netid);
		return FALSE;
	}
	
	// Return the array of groups
	return $groups;
}